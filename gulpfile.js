'use strict';

var gulp = require('gulp');
var plugins = require('gulp-load-plugins')();

function getTask(task) {
    return require('./gulp-tasks/' + task)(gulp, plugins);
}

gulp.task('clean', getTask('clean'));
gulp.task('sass', getTask('sass'));
gulp.task('script', getTask('script'));
gulp.task('img', getTask('img'));

gulp.task('dev', getTask('dev'));

gulp.task('build', ['clean', 'sass', 'script', 'img'], function () {
    gulp.dest('./dist');
});

