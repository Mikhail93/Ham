$(".slider__controller--list li:first-child").addClass("activeItem");

function slide(target) {
    $(".slider__controller--list li").removeClass("activeItem").eq(target).addClass("activeItem");
    $(".slider__header ul li").animate({
        'right': +1160 * target + 'px'
    }, 250);
}

$(".slider__controller--list li").click(function() {
    var target = $(this).index();
    slide(target);

    //Stopped auto slide when user clicked
    clearInterval(timer);
    //Then started auto slide again
    timer = setInterval(function() {
        $('#next').trigger('click');
    }, 2500);

});

$("#next").click(function() {
    var target = $(".slider__controller--list li.activeItem").index();
    if (target === $(".slider__controller--list li").length - 1) {
        target = -1;
    }
    target = target + 1;
    slide(target);

    clearInterval(timer);

    timer = setInterval(function() {
        $('#next').trigger('click');
    }, 2500);

});

$("#prev").click(function() {
    var target = $(".slider__controller--list li.activeItem").index();
    if (target === 0) {
        target = $(".slider__controller--list li").length;
    }
    target = target - 1;
    slide(target);

    //Stopped auto slide when user clicked
    clearInterval(timer);
    //Then started auto slide again
    timer = setInterval(function() {
        $('#next').trigger('click');
    }, 2500);

});

//Auto slide
var timer = null;
timer = setInterval(function() {
    $('#next').trigger('click');
}, 2500);
