(function($) {
    $(function() {

        $('ul.services__block--list').on('click', 'li:not(.active)', function() {
            $(this).addClass('active').siblings().removeClass('active')
                .closest('div.services__block').find('div.services__block--description').removeClass('active').eq($(this).index()).addClass('active');
        });

    });
})(jQuery);