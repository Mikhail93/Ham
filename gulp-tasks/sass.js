var sass = require('gulp-sass');
var cleanCSS = require('gulp-clean-css');
var rename = require('gulp-rename');
var notify = require('gulp-notify');

module.exports = function (gulp, plugins) {
    return function () {
        gulp.src('./src/scss/**/*.scss')
            .pipe(sass().on('error', notify.onError(
                {
                    message: "<%= error.message %>",
                    title: "Sass Error"
                }))
            )
            .pipe(cleanCSS())
            .pipe(rename({
                suffix: '.min'
            }))
            .pipe(gulp.dest('./dist/css'))
            .pipe( notify('SASS - хорошая работа!'));
    };
};