var clean = require('gulp-clean');

module.exports = function (gulp, plugins) {
    return function () {
        gulp.src('./dist/*', {read: false})
            .pipe(clean());
    };
};